
var exec = require('cordova/exec');

var SmartconfigResult = require('./SmartconfigResult');


function SmartconfigPlugin() { 
 console.log("SmartconfigPlugin.js: is created");
}

SmartconfigPlugin.prototype.showToast = function(aString){
 	console.log("SmartconfigPlugin.js: showToast");

 	exec(function(result){
    	 /*alert("OK" + reply);*/
   		},
 		 function(result){
   		 /*alert("Error" + reply);*/
   		},
   		"SmartconfigPlugin",aString,[]);
}


SmartconfigPlugin.prototype.search = function(successCallback,errorCallback,password){
 	console.log("SmartconfigPlugin.js: search");

 	//exec(successCallback,errorCallback,"SmartconfigPlugin","search",[]);
	
	var win = function(result) {
        console.log("SmartconfigPlugin.js: search successCallback");
        var sr  = new SmartconfigResult(result.eventType, result.progress, result.ip, result.name);
        successCallback(sr);
    };
    
    var fail = errorCallback && function(code) {
        console.log("SmartconfigPlugin.js: search errorCallback");
        //var ce = new CompassError(code);
        errorCallback(code);
    };

    exec(win, fail, "SmartconfigPlugin", "search", [password]);
    



}



//SmartconfigPlugin.prototype.stopSearching = function(successCallback){

// na razie odpowiedam korzystając z kontekstu przekazanego do funkcji search (czy to dobrze?)
SmartconfigPlugin.prototype.stopSearching = function(){
  
  console.log("SmartconfigPlugin.js: stopSearching");

  //exec(successCallback,errorCallback,"SmartconfigPlugin","search",[]);
  
  var win = function(result) {
        console.log("SmartconfigPlugin.js: stopSearching successCallback");
        //var sr  = new SmartconfigResult(result.eventType, result.progress, result.ip, result.name);
        //successCallback();
    };
    
    var fail = function(code) {
        console.log("SmartconfigPlugin.js: stopSearching errorCallback");
    };
    
    //var fail = errorCallback && function(code) {
    //    console.log("SmartconfigPlugin.js: stopSearching errorCallback");
    //    //var ce = new CompassError(code);
    //    //errorCallback(code);
    //};

    exec(win, fail, "SmartconfigPlugin", "stopSearching", []);
    



}


//SmartconfigPlugin.prototype.search = function(){
// 	console.log("SmartconfigPlugin.js: search");

 	//exec(successCallback,errorCallback,"SmartconfigPlugin","search",[]);
//}



 var smartconfigPlugin = new SmartconfigPlugin();
 module.exports = smartconfigPlugin;