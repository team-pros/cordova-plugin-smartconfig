# README #

### Aby zainstalowac: ###
```
#!sh

cordova plugin add https://bitbucket.org/team-pros/cordova-plugin-smartconfig.git
```


### Aby usunac: ###
```
#!sh

cordova plugin remove cordova-plugin-smartconfig
```



## Opis podstawowych komend: ##

#### Komenda: ####
```
#!sh

cordova plugin add https://bitbucket.org/team-pros/cordova-plugin-smartconfig.git
```

* instaluje smartconfig plugin do katalogu: *<katalog_projektu>/plugins/cordova-plugin-smartconfig*
* instaluje wszystkie potrzebne rzeczy w: *<katalog_projektu>/platforms/android*

i to wystarczy aby aplikacja zbudowała się z tym pluginem

#### Komenda: ####
```
#!sh

cordova plugin add https://bitbucket.org/team-pros/cordova-plugin-smartconfig.git --save
```
to co powyżej a dodatkowo zapisuje informacje na temat zainstalowanego plugina w: *<katalog_projektu>/config.xml*

Najfajniejsze jest to że jak zainstalujesz plugin tym drugim sposobem (tak zrobiłem w naszej aplikacji) to nawet jak nie masz aktualnie plugina w: *<katalog_projektu>/plugins/* (tak jak w naszym przypadku gdzie git nie śledzi zawartości katalogu plugins) i budujesz aplikację: *"ionic run android"* to cordova odczyta sobie z config.xml jakie pluginy są wymagane i gdzie je znaleźć i w pierwszym kroku budowania ściągnie je sobie i zainstaluje. 

#### Komenda: ####
```
#!sh

cordova plugin  
```
listuje zainstalowane pluginy


#### Komenda: ####
```
#!sh

cordova plugin remove cordova-plugin-smartconfig
```
* usuwa katalog *cordova-plugin-smartconfig* z *<katalog_projektu>/plugins/*
* usuwa wszystkie zmiany wprowadzone przez ten plugin w katalogu *<katalog_projektu>/platforms/android*
 

#### Komenda: ####
```
#!sh

cordova plugin remove cordova-plugin-smartconfig --save
```
to co powyżej a dodatkowo usuwa informacje na temat plugina z pliku konfiguracyjnego: *<katalog_projektu>/config.xml*

 