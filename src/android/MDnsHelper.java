//package pl.pros.prosapp;

import android.app.Activity;

import javax.jmdns.JmDNS;
import javax.jmdns.ServiceEvent;
import javax.jmdns.ServiceListener;
import javax.jmdns.ServiceTypeListener;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by Tomek on 2016-02-15.
 */
public class MDnsHelper implements ServiceListener, ServiceTypeListener  {


    public static final String SERVICE_TYPE = "_http._tcp.";
    //public static final String SMARTCONFIG_IDENTIFIER = "srcvers=1D90645";
    public static final String SMARTCONFIG_IDENTIFIER = "pros_id";
    
    public static final String MDNS_TAG = "mDNS";


    Activity activity;
    MDnsCallbackInterface callback;
    private JmDNS jmdns;
    private WifiManager.MulticastLock multicastLock;
    WifiManager wm;
    InetAddress bindingAddress;
    boolean isDiscovering;

    public void init(Activity activity, MDnsCallbackInterface callback) {
        this.activity = activity;
        this.callback = callback;
        isDiscovering = false;
        wm = (WifiManager) activity.getSystemService(Context.WIFI_SERVICE);
        multicastLock = wm.createMulticastLock(getClass().getName());
        multicastLock.setReferenceCounted(true);
    }

    //@Background
    public void startDiscovery() {

        Log.d(MDNS_TAG, "startDiscovery");

        if (isDiscovering)
            return;
        final InetAddress deviceIpAddress = getDeviceIpAddress(wm);
        if (!multicastLock.isHeld()){
            multicastLock.acquire();
        } else {
            Log.d(MDNS_TAG,"Muticast lock already held...");
        }
        try {
            jmdns = JmDNS.create(deviceIpAddress, "SmartConfig");
            jmdns.addServiceTypeListener(this);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (jmdns != null) {
                isDiscovering = true;
                Log.d(MDNS_TAG,"discovering services...");
            }
        }
    }

    //@Background
    public void stopDiscovery() {

        Log.d(MDNS_TAG,"stopDiscovery");

        try {
            if (!isDiscovering || jmdns == null)
                return;
            if (multicastLock.isHeld()){
                multicastLock.release();
            } else {
                Log.d(MDNS_TAG,"Multicast lock already released");
            }
            jmdns.unregisterAllServices();
            jmdns.close();
            jmdns = null;
            isDiscovering = false;
            Log.d(MDNS_TAG,"MDNS discovery stopped");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void serviceAdded(ServiceEvent service) {}

    @Override
    public void serviceRemoved(ServiceEvent service) {}

    @Override
    public void serviceResolved(ServiceEvent service) {
        if (service.getInfo().getNiceTextString().contains(SMARTCONFIG_IDENTIFIER)){
        
        //if (true) {
            Log.d(MDNS_TAG,"serviceResolved: " + service.toString());

            Log.d(MDNS_TAG, "Type = " + service.getType());
            Log.d(MDNS_TAG, "Name = " + service.getName());
            Log.d(MDNS_TAG, "Info.name = " + service.getInfo().getName());
            Log.d(MDNS_TAG, "Info.nice = " + service.getInfo().getNiceTextString());
            Log.d(MDNS_TAG, "Info.addres = " + service.getInfo().getHostAddresses()[0]);

            JSONObject deviceJSON = new JSONObject();
            try {
                deviceJSON.put("name", service.getName());
                deviceJSON.put("host", service.getInfo().getHostAddresses()[0]);
                //callback.onDeviceResolved(deviceJSON);
                //callback.onDecviceResolved(service.getName());
                callback.onDeviceResolved(service.getName(),service.getInfo().getHostAddresses()[0]);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        else {
            Log.d(MDNS_TAG,"Znalazlem ponizsze urzadzenie ale nie zawiero ono SMARTCONFIG_IDENTIFIER wiec nie zwracam");    
            Log.d(MDNS_TAG,"serviceResolved: " + service.toString());
            Log.d(MDNS_TAG, "Type = " + service.getType());
            Log.d(MDNS_TAG, "Name = " + service.getName());
            Log.d(MDNS_TAG, "Info.name = " + service.getInfo().getName());
            Log.d(MDNS_TAG, "Info.nice = " + service.getInfo().getNiceTextString());
            Log.d(MDNS_TAG, "Info.addres = " + service.getInfo().getHostAddresses()[0]);
        }

    }

    private InetAddress getDeviceIpAddress(WifiManager wifi) {
        InetAddress result = null;
        try {
            // default to Android localhost
            result = InetAddress.getByName("10.0.0.2");

            // figure out our wifi address, otherwise bail
            WifiInfo wifiinfo = wifi.getConnectionInfo();
            int intaddr = wifiinfo.getIpAddress();
            byte[] byteaddr = new byte[] { (byte) (intaddr & 0xff), (byte) (intaddr >> 8 & 0xff), (byte) (intaddr >> 16 & 0xff), (byte) (intaddr >> 24 & 0xff) };
            result = InetAddress.getByAddress(byteaddr);
        } catch (UnknownHostException ex) {
            Log.w(MDNS_TAG, String.format("getDeviceIpAddress Error: %s", ex.getMessage()));
        }

        return result;
    }

    @Override
    public void serviceTypeAdded(ServiceEvent event) {
        // TODO Auto-generated method stub
        Log.d(MDNS_TAG, "type " + event.getType());
        Log.d(MDNS_TAG, "name " + event.getName());
        if (event.getInfo() == null) Log.e("MDNS helper" , "get info == null");
        
        //Log.e("MDNS helper", "info.type " + event.getInfo().getType());
        //Log.e("MDNS helper", "info.name " + event.getInfo().getName());
        //Log.e("MDNS helper", "info.nice " + event.getInfo().getNiceTextString());

        if (event.getType().contains(SERVICE_TYPE)) {
            Log.d(MDNS_TAG, "Service listener added");
            jmdns.addServiceListener(event.getType(), this);
        }
        else {
            Log.e(MDNS_TAG, "Service listener not added");
        }
    }

    @Override
    public void subTypeForServiceTypeAdded(ServiceEvent event) {}
}
