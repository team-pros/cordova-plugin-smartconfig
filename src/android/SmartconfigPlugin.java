
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaInterface;
import android.util.Log;
import android.provider.Settings;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
 
import android.os.Handler;
import android.os.Looper;
import org.apache.cordova.PluginResult; 

import com.integrity_project.smartconfiglib.SmartConfig;
import com.integrity_project.smartconfiglib.SmartConfigListener;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import android.os.Message;

import java.util.ArrayList;


// UWAGA: po każdej zmianie plugina muszę go zapamiętać a potem zainstalować w testowaj aplikacji
//  Ta testowa aplikacja znajduje się w D:\ionic\testListy
// Muszę wejść do tego katalogu a nastepnie wydać komendy
// cordova plugin remove com.pros.smartconfigplugin
// cordova plugin add d:\ionic\SmartconfigPlugin
// ionic run android
// Teraz juz jest na gicie:
// cordova plugin remove cordona-plugin-smartconfig
// cordova plugin add https://bitbucket.org/team-pros/cordova-plugin-smartconfig.git --save
// Jak jest "--save" to orócz tego ze instaluje plugin dodaje info na temat tego plugina
// do pliku konfiguracyjnego config.xml i przy ponownym budowaniu aplikacji jak zobaczy że 
// nie ma takiego plugina to sobie sam go sciagnie


// 2016.07.16
//  Plugin nie korzysta juz z watków dostarczanych przez cordova.getThreadPool() bo jak działałem
//  w ten sposób to się wykrzaczał. Teraz tworzą swój jeden wątek WorkingThread i wszytko
//  co chcę zobić komunikuję mu przez komunikaty 


// TODO LIST
// OK Czy mogę tak zostawić Loopera - chyba muszę go zabić i zakończyć wątek. Zrobione.
// OK Dołączyłem biblioteki (patrz: plugin.xml)
//    Dołaczyć biblioteki używając <framework> a nie <lib-file> 
// OK Włączyć i wyłączyć smartconfiga
// OK Włączyć i wyłączyć MDNSa
// OK Przekazać wynik poszukiwań do JavaScriptu (JSONObject)
//      Do javascriptu wysyłam JSONObject:
//        eventType  (procent szukania czy znalezione urządzenie) 
//         progress  (int)
//         ip  (adres ip urządzenia)
//         name  (nazwa urządzenia)
// OK Przekazać parametry do Javy (JSONArray): gateway, SSID, password
//      Aby ułatwić aplikacji korzystanie z plugina sam sobie wyłuskam ssid i gateway,
//      aplikacja musi mi przekazać password jako pierwszy argument JSONArray 
// OK Założyć repozytorium dla plugina i wrzucić do pros
// OK Nie ma zwracać urządzeń które nie są smartconfigami
// OK   Czasami zwracam dwa takie same urzadzenia - musze to skontrolować i nie zwracac powtórzonych
// SPRAWDZIĆ Zrobić możliwość zatrzymania szukania. Funkcja "stop"
// OK Aplikacja testowa powinna pokazywać stan zaawansowania
//    Aplikacja testowa - dodać możliwość "STOP"  (wtedy STOPPING...)
//    Dodać password do aplikacji ionic
//    Dodać możliwość przerzucania urządzeń z listy znalezionych do listy zapamietanych


public class SmartconfigPlugin extends CordovaPlugin {
 
	public static final String TAG = "KOSA";
 
    // Zdarzenia które mogą być przekazane do js:
    public static final int PROGRESS = 1;
    public static final int DEVICE_FOUND = 2;
    public static final int SEARCH_FINISHED = 3;
    
    // komunikaty rozumiane przez working thread
    private static final int START_SEARCHING = 10;
    private static final int STOP_SERACHING = 11;
    private static final int NEXT_TICK = 12;
    
    private static final int DELAY = 1500; //milliseconds


    private String mPassword;  // hasło do sieci pzrekazane z js

    private int mTick;  
    
    private boolean searching;  // czy jestem w trakcie szukania

    private boolean stopSearching;  // flaga mówiąca o tym że użytkownik chce przerwać szukanie

    private CallbackContext searchCallbackContext;

    private SmartConfig smartConfig;  // realizuje wysyłanei w eter konfiguracji (SSID i hasło)
 
    private MDnsHelper mDnsHelper;   // realizuje nasłuchiwanie na adresie multicastowym nazw urzadzen  

    private Handler mWorkingThreadHandler = null;

    private ArrayList<String> adresyUrzadzen = null;   // zapamiętuję wszystkie nzalezione podczas aktualnego szukania
                                                  // adresy IP bo czasem zdarza się że mDNS zwróci mi 2 razy to samo
                                                  // a nie chcę zwracać dwa razy tego samego do aplikacji


	/**
	* Constructor.
	*/
	public SmartconfigPlugin() {}
 
	/**
	* Sets the context of the Command. This can then be used to do things like
	* get file paths associated with the Activity.
	*
	* @param cordova The context of the main Activity.
	* @param webView The CordovaWebView Cordova is running in.
	*/
  
	public void initialize(CordovaInterface cordova, CordovaWebView webView) {
		super.initialize(cordova, webView);
		Log.v(TAG,"Init SmartconfigPlugin");

        searching = false;
        stopSearching = false;

	}
 


	public boolean execute(final String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
         
       
        Log.v(TAG,"SmartconfigPlugin received:"+ action);	
		
        // szukam wykorzystując smartconfig
		if (action.equals("search")) {

            // jeżli jestem w trakcie szukania to musze zwrócić bład i tyle
            if (searching == true) {
                Log.v(TAG,"... NIE NIE ROBIĘ bo juz jestem w trakcie szuaknia");  
            }
            else {
                searching = true;
                stopSearching = false;

                Log.v(TAG,"... wiec rozpoczyna szukanie");  
                
                // Odczytuję hasło przekazana z js
                mPassword = args.getString(0);
                
                mTick = 0; 

                // jeżli jeszcze nie ma handlera dla workingthreada tzn że jestem tu pierwszy raz
                // i muszę utworzyc workingthread i poczekać aż on utworzy handlera    
                if (mWorkingThreadHandler == null) {
                    Log.v(TAG,"Tworze working thread");  
                    WorkingThread wt = new WorkingThread();
                    wt.start();
                    while (mWorkingThreadHandler == null) {
                        try {
                            Thread.sleep(5);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }   
                    }
                    Log.v(TAG,"Doczekalem sie handlera working thread");  
                }
                
                // tworzę listę adresów urządzeń aby zapamiętać wszytskie znalezione adresy aby nie zwracać
                // dwa razy tego samoego do js
                if (adresyUrzadzen == null) {
                    adresyUrzadzen = new ArrayList<String>();
                }
                // lista jest ważna tylko przez jedno szukanie, przed każdym szukaniem czyszczę ją
                else {
                    adresyUrzadzen.clear();
                }    

                // Abym nie dostawał komuikatów o treści:
                // W/CordovaPlugin: Attempted to send a second callback for ID: SmartconfigPlugin486197692
                //                                                                       Result was: 40
                // muszę powiedzić cordovie że będzie wile odpowiedzi od tego samego plugina
                searchCallbackContext = callbackContext;
                PluginResult result = new PluginResult(PluginResult.Status.NO_RESULT);
                result.setKeepCallback(true);
                searchCallbackContext.sendPluginResult(result);
                 
                mWorkingThreadHandler.sendMessage(Message.obtain(mWorkingThreadHandler, START_SEARCHING));

                //cordova.getThreadPool().execute(new Runnable() { } );
         
            }


		}

        // użytkownik zarządał przerwania szukania
        // (na razie jest tak że nie zapamiętuję kontekstu dla funkcji "stopSearching")
        // więc jeżeli będę coś zwracał to wykorzystując kontekst przekazany do funkcji "search"
        // Może to należy zmienić???
        else if (action.equals("stopSearching")) {
            if ((searching == false) || (stopSearching == true)) {
                Log.v(TAG,"... NIE NIE ROBIĘ bo nie jestem w trakcie szukania");  
            }
            else {
                stopSearching = true;   // użytkownik chce przerwać szuknie
                Log.v(TAG,"... więc przerywam szukanie");  

                // może sie zdarzyć (choć to bardzo mało prawdopodobne) że zaraz po tym jak
                // było "search" będzie "stopSearching" i to będzie pierwszy raz gdy nie ma jeszcz
                // wystartował jeszcze workingThread więc na wszelki wypadek tutaj sprawdzam
                // i czekam (jak jest null to zaraz powinien nie być null bo workin thread go utworzy) 
                while (mWorkingThreadHandler == null) {
                    try {
                        Thread.sleep(5);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }   
                }

                mWorkingThreadHandler.sendMessage(Message.obtain(mWorkingThreadHandler, STOP_SERACHING));

            }
        }


		// wyswietlam toast - test
		else {
			final int duration = Toast.LENGTH_SHORT;
			// Shows a toast
			Log.v(TAG,"... wiec wyswielta toasta");	
          
			cordova.getActivity().runOnUiThread(new Runnable() {
				public void run() {
					Toast toast = Toast.makeText(cordova.getActivity().getApplicationContext(), action, duration);
					toast.show();
				}
			});		
		}

		return true;
	}  // koniec execute()







    //--------------------------------------------------------------------------
    // LOCAL METHODS
    //--------------------------------------------------------------------------


    /**
     * Przygotowuję JSONObject do wysłania do js
     *
     * @return JSONObject
     */
    private JSONObject createSmartconfigJSON(int eventType,int progress, String ip, String name) {
        JSONObject smartconfigJSON = new JSONObject();

        try {
            smartconfigJSON.put("eventType", eventType);
            smartconfigJSON.put("progress", progress);
            smartconfigJSON.put("ip", ip);
            smartconfigJSON.put("name", name);
        } catch (Exception e) {
            e.printStackTrace();
        }   
        
        return smartconfigJSON;
    }

	



    class WorkingThread extends Thread {
       
        @Override
        public void run() {
            Looper.prepare();

            Log.v(TAG,"WorkingThread wystartował");

            mWorkingThreadHandler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                
                    if (msg.what == START_SEARCHING) {
                        Log.v(TAG,"WorkingThread msg == START_SEARCHING"); 

                        // SmartConfig
                        // 1) callback SmartCOnfigListener który jest wywoływany na zdarzenia
                        // 2) free data  (w przykłądzie bajt1: 0x03, bajt2: ile jeszcze bajtów, bajt3,4,5... nazwa urzadzenia
                        // 3) hasło sieci do której ma być przyłączone urządzenie
                        // 4) klucz kodujący jaki ma być urzyty do przesłania konfiguracji (musi być taki sam ustawiony w CC3200)
                        // 5) adres ip gdzie wysłać dane konfigurujące (chyba obojętnie gdzie ale generalnie gateway)
                        // 6) SSID ksici do której ma być przyłączone urządzenie
                        // 7) ? ackString - Co ma być wysłane w czasie czasie potwierdzania (przez mDNS) przez urządznie (domyślnie: ???)
                        // 8) ?

                        byte[] freeData = new byte[1];
                        freeData[0] = 0x03;

                        byte[] paddedEncryptionKey = null;

                        //String password = "kosa1980"; //"kosa1234";
                        //String gateway = "192.168.2.1";
                        //String ssid = "skosa" ; //"axis_konstr";
                        String password = mPassword;

                        // Od wifimanagera dowiem się jakie jest ssid i gateway
                        WifiManager wifimanager = (WifiManager) cordova.getActivity().getSystemService(Context.WIFI_SERVICE);
                        int adres = wifimanager.getDhcpInfo().gateway;
                        String gateway =  ((adres>>>0)&0xff) + "." + ((adres>>>8)&0xff) + "." + ((adres>>>16)&0xff) + "." + ((adres>>>24)&0xff);
                        
                        String ssid = wifimanager.getConnectionInfo().getSSID();
                        ssid = ssid.replace("\"","");
                        
                        Log.v(TAG,"Wysyłam konfigurację. gateway= " + gateway + " ssid= " + ssid + " password= " + password); 

                        smartConfig = null;
                        SmartConfigListener smartConfigListener = new SmartConfigListener() {
                            @Override
                            public void onSmartConfigEvent(SmtCfgEvent event, Exception e) {}
                        };

                        try {
                            smartConfig = new SmartConfig(smartConfigListener, freeData, password, paddedEncryptionKey, gateway, ssid, (byte) 0, "");
                            smartConfig.transmitSettings();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        try {

                            MDnsCallbackInterface mDnsCallback = new MDnsCallbackInterface() {
                                @Override
                                public void onDeviceResolved(String name, String ip) {
                                 
                                    Log.v(TAG,"Znalazlem urzadzenie nazwa=" + name + " ip=" + ip); 
                                 
                                    // sprawdzam czy nie mam już takiego urządzenia
                                    // i jeżli nie to wysyłam odpowiedź
                                    if (adresyUrzadzen.contains(ip)==false) {
                                    // zapamiętuję to znalezione urządznie
                                    adresyUrzadzen.add(ip);    

                                    // wysyłam odpowiedź
                                    JSONObject smartconfigJSON = createSmartconfigJSON(DEVICE_FOUND,0,ip,name);
                                    PluginResult result = new PluginResult(PluginResult.Status.OK,smartconfigJSON);
                                    result.setKeepCallback(true);
                                    searchCallbackContext.sendPluginResult(result);
                                    } 

                                    
                                }
                            };

                            mDnsHelper = new MDnsHelper();
                            Log.v(TAG,"mDnsHelper.init"); 
                            mDnsHelper.init(cordova.getActivity(), mDnsCallback);
                            Log.v(TAG,"mDnsHelper.startDiscovery"); 
                            mDnsHelper.startDiscovery();
                            Log.v(TAG,"mDnsHelper.startDiscovery zakonczone"); 

                        } catch (Exception e) {
                            e.printStackTrace();
                        }           

                        mWorkingThreadHandler.sendMessageDelayed(Message.obtain(mWorkingThreadHandler, NEXT_TICK),DELAY);

                    }  // koniec if (msg.arg1 == START_SEARCHING)
                



                    if (msg.what == STOP_SERACHING) {
                        // wysyłam odpowiedź
                        Log.v(TAG,"WorkingThread msg == STOP_SERACHING"); 
                        
                        JSONObject smartconfigJSON = createSmartconfigJSON(PROGRESS,90,"","");
                        PluginResult result = new PluginResult(PluginResult.Status.OK,smartconfigJSON);
                        result.setKeepCallback(true);
                        searchCallbackContext.sendPluginResult(result);
                        
                        // stopuję smartconfig
                        if (smartConfig!=null) {
                            try {
                                smartConfig.stopTransmitting();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }   
                        } 

                        //stopuję mdnsa    
                        if (mDnsHelper!=null) mDnsHelper.stopDiscovery();


                        smartconfigJSON = createSmartconfigJSON(SEARCH_FINISHED,100,"","");
                        result = new PluginResult(PluginResult.Status.OK,smartconfigJSON);
                        result.setKeepCallback(true);
                        searchCallbackContext.sendPluginResult(result);
                    
                        searching = false;
                        stopSearching = false;
                        // na koniec wychodzę z loopera aby wątek mógł się zakończyć
                        //Log.v(TAG,"h.getLooper().quit()");
                        //h.getLooper().quit();         
                    }  //koniec if (msg.arg1 == STOP_SERACHING)



                    if (msg.what == NEXT_TICK) {
                        
                        Log.v(TAG,"WorkingThread msg == NEXT_TICK"); 
                        mTick++;
                                             
                        // jezli już przerwałem poszukiwania lub jestem w trakcie przerywania to
                        // nie mam już tu nic do zrobienia
                        if ((searching == false) || (stopSearching == true)) {}
                        else {
                            
                            // jeżli jest to tick numer 5 (jestem w połowie szukania) to przerywam na chwilę
                            // mDNS aby wznowić go w następnym ticku
                            // Robię w ten sposób aby mDNS jeszce raz zapytał o urządzenia w sieci 
                            // (bez tego mDNS nie wyłapywał urządzeń którym zajęło wiele czasu skonfigurowanie się w sieci)
                            if (mTick == 5) {
                                JSONObject smartconfigJSON = createSmartconfigJSON(PROGRESS,50,"","");
                                PluginResult result = new PluginResult(PluginResult.Status.OK,smartconfigJSON);
                                result.setKeepCallback(true);
                                searchCallbackContext.sendPluginResult(result);    

                                // zakładam że stopowanie mDNS trwa mniej/wiecej jeden tick    
                                if (mDnsHelper!=null) {
                                    mDnsHelper.stopDiscovery();
                                    mDnsHelper.startDiscovery();
                                }

                                smartconfigJSON = createSmartconfigJSON(PROGRESS,60,"","");
                                result = new PluginResult(PluginResult.Status.OK,smartconfigJSON);
                                result.setKeepCallback(true);
                                searchCallbackContext.sendPluginResult(result); 
                                mTick = 6;

                                mWorkingThreadHandler.sendMessageDelayed(Message.obtain(mWorkingThreadHandler, NEXT_TICK),DELAY);        

                            }    

                            // jeżli jeszcze nie upłynął cały czas to wysyłam info że upłynąl kolejny kawałek
                            // czasu i usypiam wątek na jakiś czas
                            else if (mTick < 9) {
                                // wysyłam odpowiedź
                                JSONObject smartconfigJSON = createSmartconfigJSON(PROGRESS,(mTick*10),"","");
                                PluginResult result = new PluginResult(PluginResult.Status.OK,smartconfigJSON);
                                result.setKeepCallback(true);
                                searchCallbackContext.sendPluginResult(result);

                                mWorkingThreadHandler.sendMessageDelayed(Message.obtain(mWorkingThreadHandler, NEXT_TICK),DELAY);

                            }   
                            // a jeżli upłynąl już czas szukania to wysyłam info że koniec szukania 
                            // i stopuję smartconfiga i mdnsa
                            else {
                                stopSearching = true;  // ustawiam flagę stopSearching aby już nie 
                                                       // było można wysłać message staop serching 
                                                       // (jakby się zdarzyło że akurat użytkownik będzie chciał przerwać )
                                mWorkingThreadHandler.sendMessage(Message.obtain(mWorkingThreadHandler, STOP_SERACHING));
                            }

                        }  // koniec dla else if ((searching == false) || (stopSearching == true)) {}
                    }  //koniec if (msg.arg1 == NEXT_TICK)
    
                }  // koniec public void handleMessage(Message msg)
            
            };   // koniec new Handler()



            Looper.loop();
        }
    }  


}    // koniec class SmartconfigPlugin